

public class Program {
	public static void main(String[] args) {
		Item apple = new Item("apple", 0.25);
		Item pear = new Item("pear", 0.15);
		Item grape = new Item("grape", 0.67);
		
		Basket.addToBasket(apple, 20);
		Basket.addToBasket(apple);
		Basket.addToBasket(pear);
		Basket.addToBasket(grape);
		
		System.out.println(Basket.viewBasket());
		
		Basket.removeFromBasket(apple, 20);
		Basket.removeFromBasket(apple, 20);
		Basket.removeFromBasket(grape);
		Basket.removeFromBasket(grape);
		System.out.println(Basket.viewBasket());
		
		Basket.undo();
		Basket.undo();
		Basket.undo();
		Basket.undo();
		Basket.undo();
		Basket.undo();
		System.out.println(Basket.viewBasket());
		
		Basket.checkout();
		System.out.println(Basket.viewBasket());
	}
}
