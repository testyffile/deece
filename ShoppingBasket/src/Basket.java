import java.util.ArrayList;
import java.util.Stack;


public abstract class Basket {

	private static ArrayList<Item> items = new ArrayList<Item> ();
	private static double totalPrice;
	private static Stack<String> methodTrace = new Stack<String>();
	private static Stack<Item> itemTrace = new Stack<Item>();
	private static Stack<Integer> quantityTrace = new Stack<Integer>();
	private static boolean isUndo = false;


	//adds items to basket with a quantity variable
	public static void addToBasket(Item item, int i) {
		//Checks if basket already contains item, if false item is added to basket and total price is increased
		if(!items.contains(item)) {
			items.add(item);
			item.setQuantity(i);
			totalPrice += item.getPrice() * item.getQuantity();
		}
		//the quantity of the item is given a new value and the total price is increased
		else {
			int newQuantity = item.getQuantity() + i;
			item.setQuantity(newQuantity);
			totalPrice += item.getPrice() * i;
		}
		//adds the values to stack traces if not an undo action
		if(!isUndo) {
			itemTrace.push(item);
			methodTrace.push("ATB");
			quantityTrace.push(i);
		}
		isUndo = false;
	}

	//adds items without a quantity variable, quantity is set to 1
	public static void addToBasket(Item item) {
		addToBasket(item, 1);
	}

	//removes item from basket using a variable quantity
	public static void removeFromBasket(Item item, int i) {

		if(items.contains(item)){
			//if item is in basket and the quantity to be removed equals or exceeds the quantity currently in basket then the item is removed from basket
			if (i >= item.getQuantity()) {
				i = item.getQuantity();
				items.remove(item);
				totalPrice -= item.getPrice() * item.getQuantity();
			}
			//else the quantity of the item is reduced by the specified quantity and a new quantity is set
			else {
				int newQuantity = item.getQuantity() - i;
				item.setQuantity(newQuantity);
				totalPrice -= item.getPrice() * i;
			}
			//add values to stack traces if not an undo action
			if(!isUndo) { 
				itemTrace.push(item);
				methodTrace.push("RFB");
				quantityTrace.push(i);
			}
			isUndo = false;
		}
		//If item is not in basket prints appropriate message
		else {
			System.out.printf("Error - Item: %s is not in basket\n", item.getName());
		}
	}

	//removes items without a quantity variable, quantity is set to max quantity of item in basket
	public static void removeFromBasket(Item item) {
		removeFromBasket(item, item.getQuantity());
	}

	//prints the items, price, quantity and total price of items in basket
	public static String viewBasket() {
		String viewBasket = "";
		if(methodTrace.empty()) {
			viewBasket = "Basket is empty";
			//System.out.println("Basket is empty");
		}
		else {
			for(Item i : items) {
				viewBasket += String.format("%s\t�%.2f\tQuantity: %s\n", i.getName(), i.getPrice(), i.getQuantity());
				//System.out.printf("%s\t�%.2f\tQuantity: %s\n", i.getName(), i.getPrice(), i.getQuantity());
			}
			viewBasket += String.format("\nTotal: �%.2f\n\n", totalPrice);
			//System.out.printf("\nTotal: �%.2f\n\n", totalPrice);
		}
		return viewBasket;
	}

	// returns number of items in basket
	public int basketSize() {
		return items.size();
	}

	//performs the opposite action of the last method performed
	public static void undo() {
		if(!methodTrace.empty()) {
			isUndo = true;
			String type = methodTrace.pop();
			switch (type) {
			case "ATB" :
				System.out.printf("UNDO performed - Item %s quantity %s removed from basket\n\n", itemTrace.peek().getName(), quantityTrace.peek());
				Basket.removeFromBasket(itemTrace.pop(), quantityTrace.pop());
				break;
			case "RFB" :
				System.out.printf("UNDO performed - Item %s quantity %s added to basket\n\n", itemTrace.peek().getName(), quantityTrace.peek());
				Basket.addToBasket(itemTrace.pop(), quantityTrace.pop());
				break;
			}
		}
	}

	//Empties basket, clears stacks traces and sets total price to 0
	public static String checkout() {
		String status = "";
		if(!methodTrace.empty()) {
			items.clear();
			methodTrace.clear();
			itemTrace.clear();
			quantityTrace.clear();
			totalPrice = 0;
			status = "Items purchased";
		}
		else {
			status = "Can not proceed with empty basket";
		}
		return status;
	}

}
